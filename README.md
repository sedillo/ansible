# ansible

Creating a directory for ansible scripts and configuration 

## Getting started

```bash
sudo apt-get update -y
sudo apt-get install -y ansible
```

## Making a new role

Execute at the ~/ansible directory
```bash
ansible-galaxy init ROLE_NAME
```

